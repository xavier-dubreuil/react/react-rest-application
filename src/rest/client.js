import {generateUrl} from '../util/url-generator';
import {jsonClient} from './json-client';

export const AUTH = 'AUTH';
export const LIST = 'LIST';
export const GET = 'GET';
export const CREATE = 'CREATE';
export const UPDATE = 'UPDATE';
export const DELETE = 'DELETE';

export function generateClient(apiUrl, httpClient = jsonClient, logger = false) {

    const convertRESTRequestToHTTP = (type, resource, params_resources, params) => {
        let url = apiUrl + generateUrl(resource, params_resources);
        const options = {};
        switch (type) {
        case AUTH: {
            options.method = 'POST';
            options.body = JSON.stringify(params.data);
            break;
        }
        case LIST: {
            options.method = 'GET';
            break;
        }
        case GET:
            options.method = 'GET';
            break;
        case UPDATE:
            options.method = 'PUT';
            options.body = JSON.stringify(params.data);
            break;
        case CREATE:
            options.method = 'POST';
            options.body = JSON.stringify(params.data);
            break;
        case DELETE:
            options.method = 'DELETE';
            break;
        default:
            throw new Error(`Unsupported fetch action type ${type}`);
        }
        return { url, options };
    };

    /**
     * @param {string} type Request type, e.g GET_LIST
     * @param {string} resource Resource name, e.g. "posts"
     * @param {Object} payload Request parameters. Depends on the request type
     * @returns {Promise} the Promise for a REST response
     */
    return (type, resource, params_resources = {}, params = {}) => {
        const { url, options } = convertRESTRequestToHTTP(type, resource, params_resources, params);
        if (logger) {

            console.group('REST Client request'); // eslint-disable-line
            console.log('type', type); // eslint-disable-line
            console.log('resource', resource); // eslint-disable-line
            console.log('params_resources', params_resources); // eslint-disable-line
            console.log('params', params); // eslint-disable-line
            console.log('url', url); // eslint-disable-line
            console.log('options', options); // eslint-disable-line
            console.groupEnd(); // eslint-disable-line
        }
        return httpClient(url, options)
            .then(
                (response) => {
                    const { headers, json } = response;
                    if (logger) {
                        console.group('REST Client response'); // eslint-disable-line
                        console.log('resource', resource); // eslint-disable-line
                        console.log('params_resources', params_resources); // eslint-disable-line
                        console.log('url', url); // eslint-disable-line
                        console.log('headers', headers); // eslint-disable-line
                        console.log('response', json); // eslint-disable-line
                        console.groupEnd(); // eslint-disable-line
                    }
                    return Promise.resolve(json);
                },
                (response) => {
                    const {headers, json} = response;
                    if (logger) {
                        console.group('REST Client response'); // eslint-disable-line
                        console.log('resource', resource); // eslint-disable-line
                        console.log('params_resources', params_resources); // eslint-disable-line
                        console.log('url', url); // eslint-disable-line
                        console.log('headers', headers); // eslint-disable-line
                        console.log('response', json); // eslint-disable-line
                        console.groupEnd(); // eslint-disable-line
                    }
                    return Promise.reject(response);
                }
            );
    };
}