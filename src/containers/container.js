import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {invalidateElement, getElements, getElement, createElement, updateElement, deleteElement} from '../actions';
import {generateUrl, generateHash} from '../util';

const getDefault = (resource) => {
    return resource.collection ? [] : {};
};

class Container extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};

        const {resources} = props.viewComponent;

        if (resources) {
            for (let name in resources) {
                if (resources.hasOwnProperty(name)) {
                    this.state[name] = getDefault(resources[name]);
                }
            }
        }
    }

    setStateFromProps(props) {
        for (let name in props.resources) {
            if (props.resources.hasOwnProperty(name)) {
                if (!this.state.hasOwnProperty(name) ||
                    this.state[name] === props.resources[name].default ||
                    Object.keys(this.state[name]).length === 0) {
                    this.setState({
                        [name]: props.resources[name].content,
                    });
                }
            }
        }
    }

    componentDidMount() {
        const {resources} = this.props.viewComponent;

        this.setStateFromProps(this.props);

        if (resources) {
            for (let name in resources) {
                if (resources.hasOwnProperty(name)) {
                    if (resources[name].autoFetch) {
                        const invalidate = resources[name].forceInvalidate || false;
                        this.getResource(name, invalidate);
                    }
                }
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setStateFromProps(nextProps);
    }

    goto(path, params = {}) {
        const {history} = this.context.router;
        history.push(generateUrl(path, {...this.props.match.params, ...params}));
    }

    handleChange(event) {
        const target = event.target;
        const name = target.name.split('.');

        let value = '';
        if (target.type === 'select-multiple') {
            value = [];
            const options = target.options;
            for (let i = 0; i < options.length; i++) {
                if (options.hasOwnProperty(i) && options[i].selected) {
                    if (options[i].getAttribute('data-value')) {
                        value.push(JSON.parse(options[i].getAttribute('data-value')));
                    } else {
                        value.push(options[i].value);
                    }
                }
            }
        } else if (target.type === 'select-one') {
            const option = target.options[target.options.selectedIndex];
            if (option.getAttribute('data-value')) {
                value = JSON.parse(option.getAttribute('data-value'));
            } else {
                value = option.value;
            }
        } else if (target.type === 'checkbox') {
            value = target.checked;
        } else if (target.type === 'file') {
            if (target.multiple) {
                value = target.files;
            } else {
                value = target.files[0];
            }
        } else {
            value = target.value;
        }

        if (name.length === 1) {
            this.setState({
                [name[0]]: value
            });
        } else if (name.length === 2) {
            this.setState({
                [name[0]]: {
                    ...this.state[name[0]],
                    [name[1]]: value
                }
            });
        }
    }

    generateUrl(resource, params) {
        const parameters = Object.assign({}, this.props.match.params);
        Object.assign(parameters, params);
        return generateUrl(resource, parameters);
    }

    getParams(name) {
        const {resources} = this.props.viewComponent;

        let res = {
            resource: null,
            state: this.state[name] || null,
        };

        if (resources) {
            res['resource'] = resources[name] || null;
        }
        return res;
    }

    getResource(name, invalidate = false) {
        const {resource} = this.getParams(name);
        if (resource) {
            const {dispatch, match} = this.props;
            if (resource.collection) {
                return dispatch(getElements(resource.path, match.params, resource.hash || false, invalidate))
                    .then((json) => Promise.resolve(json))
                    .catch((json) => Promise.reject(json));
            }
            return dispatch(getElement(resource.path, match.params, resource.hash || false, invalidate))
                .then((json) => Promise.resolve(json))
                .catch((json) => Promise.reject(json));
        }
    }

    createResource(name) {
        const {resource, state} = this.getParams(name);
        if (resource && state && !resource.collection) {
            const {dispatch, match} = this.props;
            return dispatch(createElement(resource.path, state, match.params, resource.hash || false))
                .then((json) => Promise.resolve(json))
                .catch((json) => Promise.reject(json));
        }
    }

    updateResource(name) {
        const {resource, state} = this.getParams(name);
        if (resource && state && !resource.collection) {
            const {dispatch, match} = this.props;
            return dispatch(updateElement(resource.path, state, match.params, resource.hash || false))
                .then((json) => Promise.resolve(json))
                .catch((json) => Promise.reject(json));
        }
    }

    deleteResource(name) {
        const {resource, state} = this.getParams(name);
        if (resource && state && !resource.collection) {
            const {dispatch, match} = this.props;
            return dispatch(deleteElement(resource.path, state.id, match.params, resource.hash || false))
                .then((json) => Promise.resolve(json))
                .catch((json) => Promise.reject(json));
        }
    }

    invalidateResource(name) {
        const {resource, state} = this.getParams(name);
        if (resource && state) {
            const {dispatch, match} = this.props;
            return dispatch(invalidateElement(resource.path, match.params, resource.hash || false));
        }
    }

    render() {
        return (
            <this.props.viewComponent
                propsData={this.props.resources}
                stateData={this.state}
                goto={this.goto.bind(this)}
                handleChange={this.handleChange.bind(this)}
                fetch={this.getResource.bind(this)}
                create={this.createResource.bind(this)}
                update={this.updateResource.bind(this)}
                delete={this.deleteResource.bind(this)}
                invalidate={this.invalidateResource.bind(this)}
                url={this.generateUrl.bind(this)}
                {...this.props}
            />
        );
    }
}

Container.propTypes = {
    viewComponent: PropTypes.func.isRequired,
    path: PropTypes.string,
};

Container.contextTypes = {
    router: PropTypes.shape({
        history: PropTypes.object.isRequired,
    }),
};

function mapStateToProps(state, ownProps) {
    const {resources} = ownProps.viewComponent;

    if (!resources) {
        return {};
    }

    let results = {};

    for (let name in resources) {
        if (resources.hasOwnProperty(name)) {
            const resource = resources[name];
            const hash = resource.hash ? resource.hash : generateHash(resource.path, ownProps.match.params);
            const reducer = state.DataReducer[hash] || {};
            results = {
                ...results,
                [name]: {
                    content: resource.default || getDefault(resource),
                    isFetching: false,
                    didInvalidate: false,
                    error: {},
                    default: resource.default || getDefault(resource),
                    ...reducer
                },
            };
        }
    }

    return {
        resources: results
    };
}

export default withRouter(connect(mapStateToProps)(Container));
