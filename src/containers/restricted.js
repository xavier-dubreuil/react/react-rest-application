import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {getAuthClient} from '../application';

class Restricted extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            access: 'granted',
        };
    }

    componentDidMount() {
        const authClient = getAuthClient();
        this.setState({access: authClient('AUTH_GRANT', {resource: this.props.resource}) ? 'granted' : 'denied'});
        authClient('AUTH_GRANT', {path: this.props.match.path})
            .then(
                () => {
                    this.setState({access: 'granted'});
                },
                () => {
                    this.setState({access: 'refused'});
                }
            );
    }

    render() {
        if (this.state.access === 'granted') {
            return (
                <this.props.component {...this.props} />
            );
        } else if (this.state.access === 'granted') {
            return (
                <div>Access denied</div>
            );
        }
        return (
            <div/>
        );
    }
}

Restricted.PropTypes = {
    component: PropTypes.func.isRequired,
    resource: PropTypes.string.isRequired,
};

Restricted.contextTypes = {
    router: PropTypes.shape({
        history: PropTypes.object.isRequired,
    }),
};

export default withRouter(connect()(Restricted));
