import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {disconnect} from '../actions';

class LogoutContainer extends React.Component {

    componentDidMount() {
        const {dispatch} = this.props;
        dispatch(disconnect())
            .then(() => {
                const {history} = this.context.router;
                history.push('/login');
            })
        ;
    }
    render() {
        return (
            <div/>
        );
    }
}

LogoutContainer.PropTypes = {
    logoutComponent: PropTypes.func
};

LogoutContainer.contextTypes = {
    router: PropTypes.shape({
        history: PropTypes.object.isRequired,
    }),
};

export default withRouter(connect()(LogoutContainer));
