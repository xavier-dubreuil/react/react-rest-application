import React from 'react';
import PropTypes from 'prop-types';
import {Route} from 'react-router-dom';
import Container from './container';

class RouteContainer extends React.Component {

    render() {

        const routeProps = {
            computedMatch: this.props.computedMatch,
            path: this.props.path,
            exact: this.props.exact,
            strict: this.props.strict,
            sensitive: this.props.sensitive,
            location: this.props.location,
        };

        return (
            <Route {...routeProps} component={props => <Container viewComponent={this.props.component} {...props}/>} />
        );
    }
}

RouteContainer.defaultProps = {
    exact: false,
    strict: false,
};

RouteContainer.propTypes = {
    computedMatch: PropTypes.object,
    path: PropTypes.string,
    exact: PropTypes.bool,
    strict: PropTypes.bool,
    sensitive: PropTypes.bool,
    component: PropTypes.func,
    location: PropTypes.object
};

export default RouteContainer;