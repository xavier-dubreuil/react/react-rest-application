import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {authenticate} from '../actions';

class LoginContainer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        };
    }

    handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            ...this.state,
            [name]: value
        });
    }

    connect() {
        const {history} = this.context.router;
        authenticate(this.state.username, this.state.password)
            .then(() => {
                history.push('/');
            });
    }

    render() {
        return (
            <this.props.loginComponent
                username={this.state.username}
                password={this.state.password}
                handleChange={this.handleChange.bind(this)}
                authenticate={this.connect.bind(this)}/>
        );
    }
}

LoginContainer.propTypes = {
    loginComponent: PropTypes.func.isRequired,
};

LoginContainer.contextTypes = {
    router: PropTypes.shape({
        history: PropTypes.object.isRequired,
    }),
};


export default withRouter(connect()(LoginContainer));