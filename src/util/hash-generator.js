export function generateHash(resource, params = {}) {
    let resources = resource.replace(/^\/+|\/+$/g, '').split('/');
    for (let res in resources) {
        if (resources.hasOwnProperty(res)) {
            if (resources[res].substr(0, 1) === ':') {
                if (params.hasOwnProperty(resources[res].substr(1))) {
                    resources[res] = params[resources[res].substr(1)];
                }
            }
        }
    }
    return resources.join('-');
}

export function generateParentHash(resource, params = {}) {
    let resources = resource.replace(/^\/+|\/+$/g, '').split('/');
    resources.pop();
    for (let res in resources) {
        if (resources.hasOwnProperty(res)) {
            if (resources[res].substr(0, 1) === ':') {
                if (params.hasOwnProperty(resources[res].substr(1))) {
                    resources[res] = params[resources[res].substr(1)];
                }
            }
        }
    }
    return resources.join('-');
}
