export function generateUrl(resource, params = {}) {
    let resources = resource.split('/');
    for (let res in resources) {
        if (resources.hasOwnProperty(res)) {
            if (resources[res].substr(0, 1) === ':') {
                if (params.hasOwnProperty(resources[res].substr(1))) {
                    resources[res] = params[resources[res].substr(1)];
                }
            }
        }
    }
    return resources.join('/');
}
