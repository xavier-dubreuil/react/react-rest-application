import {
    REQUEST_DATA,
    RECEIVE_DATA,
    FAIL_DATA,
    INVALIDATE_DATA,
    INVALIDATE_ALL
} from '../actions/data-actions';

const initialState = {
    isFetching: false,
    didInvalidate: false,
};

function reducer(state = initialState, action) {

    switch (action.type) {
    case INVALIDATE_DATA:
        delete state.content;
        delete state.error;
        return Object.assign({}, state, {
            didInvalidate: true
        });
    case REQUEST_DATA:
        return Object.assign({}, state, {
            isFetching: true,
            didInvalidate: false
        });
    case RECEIVE_DATA:
        return Object.assign({}, state, {
            isFetching: false,
            content: action.content
        });
    case FAIL_DATA:
        return Object.assign({}, state, {
            isFetching: false,
            error: action.error,
        });
    default:
        return state;
    }
}

function DataReducer(state = {}, action) {
    switch (action.type) {
    case INVALIDATE_DATA:
    case REQUEST_DATA:
    case RECEIVE_DATA:
    case FAIL_DATA:
        return Object.assign({}, state, {
            [action.hash]: reducer(state[action.hash], action)
        });
    case INVALIDATE_ALL:
        return {};
    default:
        return state;
    }
}

export default DataReducer;