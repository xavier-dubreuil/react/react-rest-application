import {createStore, applyMiddleware} from 'redux';
import {combineReducers} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';
import {DataReducer} from './reducers';

export default function configureStore(customReducers, preloadedState, reduxLogger = false) {

    const rootReducer = combineReducers({
        DataReducer,
        ...customReducers
    });

    let middlewares = [];
    middlewares.push(thunkMiddleware);

    if (reduxLogger) {
        middlewares.push(createLogger());
    }

    return createStore(
        rootReducer,
        preloadedState,
        applyMiddleware(...middlewares)
    );
}