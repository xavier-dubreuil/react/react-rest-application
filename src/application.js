import React from 'react';
import PropTypes from 'prop-types';
import {Router, Switch, Route} from 'react-router-dom';
import createHashHistory from 'history/createHashHistory';
import {Provider} from 'react-redux';
import configureStore from './configureStore';
import {setLogger} from './actions/data-actions'
import LoginContainer from './containers/login';
import LogoutContainer from './containers/logout';

let store;
let restClient;
let authClient;
const history = createHashHistory();

export const getStore = () => {
    return store;
};
export const getRestClient = () => {
    return restClient;
};
export const getAuthClient = () => {
    return authClient;
};
export const getHistory = () => {
    return history;
};


export class Application extends React.Component {


    render() {


        store = configureStore(this.props.customReducers, this.props.initialState, this.props.reduxLogger);
        if (this.props.storeSubscriber) {
            store.subscribe(this.props.storeSubscriber);
        }
        restClient = this.props.restClient;
        authClient = this.props.authClient;

        let children = [];
        if (Array.isArray(this.props.children)) {
            children = this.props.children;
        } else if (this.props.children !== undefined) {
            children = [this.props.children];
        }

        setLogger(this.props.actionLogger);

        return (
            <Provider store={store}>
                <Router history={history}>
                    <Switch>
                        <Route exact path="/login" component={() => {
                            return (<LoginContainer loginComponent={this.props.loginComponent} />);
                        }}/>
                        <Route exact path="/logout" component={() => {
                            return (<LogoutContainer logoutComponent={this.props.logoutComponent} />);
                        }}/>
                        <Route path="/" component={() => {return (
                            <this.props.layoutComponent>
                                {this.props.children}
                                {/*<Switch>*/}
                                    {/*{children.map((child) => {*/}
                                        {/*const exact = child.props.exact || false;*/}
                                        {/*return (*/}
                                            {/*<Route key={child.props.path} path={child.props.path} exact={exact}>*/}
                                                {/*{child}*/}
                                            {/*</Route>*/}
                                        {/*);*/}
                                    {/*})}*/}
                                {/*</Switch>*/}
                            </this.props.layoutComponent>
                        );}} />
                    </Switch>
                </Router>
            </Provider>
        );
    }
}

Application.defaultProps = {
    customReducers: {},
    customSaga: [],
    initialState: {},
    reduxLogger: false,
    actionLogger: false,
};

Application.propTypes = {
    restClient: PropTypes.func.isRequired,
    authClient: PropTypes.func,
    customReducers: PropTypes.object,
    customSaga: PropTypes.array,
    storeSubscriber: PropTypes.func,
    initialState: PropTypes.object,
    reduxLogger: PropTypes.bool,
    loginComponent: PropTypes.func.isRequired,
    logoutComponent: PropTypes.func,
    layoutComponent: PropTypes.func.isRequired,
    actionLogger: PropTypes.bool,
};
