import {generateHash, generateParentHash} from '../util';
import {getRestClient} from '../application';
export const REQUEST_DATA = 'REQUEST_DATA';
export const RECEIVE_DATA = 'RECEIVE_DATA';
export const FAIL_DATA = 'FAIL_DATA';
export const INVALIDATE_DATA = 'INVALIDATE_DATA';
export const INVALIDATE_ALL = 'INVALIDATE_ALL';

let logger = false;

export function setLogger(value) {
    logger = value;
}

function requestApi(hash) {
    return {
        type: REQUEST_DATA,
        hash: hash
    };
}

function receiveApi(hash, content) {
    return {
        type: RECEIVE_DATA,
        hash: hash,
        content: content,
    };
}

function failApi(hash, error) {
    return {
        type: FAIL_DATA,
        hash: hash,
        error: error,
    };
}

function invalidateApi(hash, error) {
    return {
        type: INVALIDATE_DATA,
        hash: hash,
        error: error,
    };
}

export function invalidateAll() {
    return {
        type: INVALIDATE_ALL,
    };
}

function shouldFetchApi(state, hash) {
    const {DataReducer} = state;
    if (!DataReducer.hasOwnProperty(hash)) {
        return true;
    } else if (DataReducer[hash].isFetching) {
        return false;
    } else if (!DataReducer[hash].hasOwnProperty('content')) {
        return true;
    }
    return DataReducer[hash].didInvalidate;
}

export function invalidateElement(resource, params = {}, hash = false) {
    hash = hash ? hash : generateHash(resource, params);

    return (dispatch) => {
        dispatch(invalidateApi(hash));
    };
}

export function getElements(resource, params = {}, hash = false, invalidate = false) {
    const restClient = getRestClient();
    hash = hash ? hash : generateHash(resource, params);

    if (logger) {
        console.group('REST Action - getElements'); // eslint-disable-line
        console.log('resource', resource); // eslint-disable-line
        console.log('params', params); // eslint-disable-line
        console.log('invalidate', invalidate); // eslint-disable-line
        console.groupEnd(); // eslint-disable-line
    }
    return (dispatch, getState) => {
        return new Promise ((resolve, reject) => {
            if (invalidate) {
                dispatch(invalidateApi(hash));
            }
            if (shouldFetchApi(getState(), hash)) {
                dispatch(requestApi(hash));
                return restClient('LIST', resource, params)
                    .then((json) => {
                        dispatch(receiveApi(hash, json));
                        resolve(json);
                    })
                    .catch((json) => {
                        dispatch(failApi(hash, json));
                        reject(json);
                    });
            } else {
                resolve(getState().DataReducer[hash].content);
            }
        });
    };
}

export function getElement(resource, params = {}, hash = false, invalidate = false) {
    const restClient = getRestClient();
    hash = hash ? hash : generateHash(resource, params);

    if (logger) {
        console.group('REST Action - getElement'); // eslint-disable-line
        console.log('resource', resource); // eslint-disable-line
        console.log('params', params); // eslint-disable-line
        console.log('invalidate', invalidate); // eslint-disable-line
        console.groupEnd(); // eslint-disable-line
    }

    return (dispatch, getState) => {
        return new Promise ((resolve, reject) => {

            if (invalidate) {
                dispatch(invalidateApi(hash));
            }
            if (shouldFetchApi(getState(), hash)) {
                dispatch(requestApi(hash));
                return restClient('GET', resource, params)
                    .then((json) => {
                        dispatch(receiveApi(hash, json));
                        resolve(json);
                    })
                    .catch((json) => {
                        dispatch(failApi(hash, json));
                        reject(json);
                    });
            } else {
                resolve(getState().DataReducer[hash].content);
            }
        });
    };
}

export function createElement(resource, element, params = {}, hash = false) {
    const restClient = getRestClient();
    hash = hash ? hash : generateHash(resource, params);

    if (logger) {
        console.group('REST Action - addElement'); // eslint-disable-line
        console.log('resource', resource); // eslint-disable-line
        console.log('params', params); // eslint-disable-line
        console.log('element', element); // eslint-disable-line
        console.groupEnd(); // eslint-disable-line
    }

    return (dispatch) => {
        dispatch(requestApi(hash));
        return new Promise ((resolve, reject) => {
            restClient('CREATE', resource, params, {data: element})
                .then((json) => {
                    dispatch(receiveApi(hash, json));
                    resolve(json);
                })
                .catch((json) => {
                    dispatch(failApi(hash, json));
                    reject(json);
                });
        });
    };
}

export function updateElement(resource, element, params = {}, hash = false) {
    const restClient = getRestClient();
    hash = hash ? hash : generateHash(resource, params);

    if (logger) {
        console.group('REST Action - updateElement'); // eslint-disable-line
        console.log('resource', resource); // eslint-disable-line
        console.log('params', params); // eslint-disable-line
        console.log('element', element); // eslint-disable-line
        console.groupEnd(); // eslint-disable-line
    }

    return (dispatch) => {
        return new Promise ((resolve, reject) => {
            restClient('UPDATE', resource, params, {data: element})
                .then(json => {
                    dispatch(receiveApi(hash, json));
                    resolve(json);
                })
                .catch((json) => {
                    dispatch(failApi(hash, json));
                    reject(json);
                });
        });
    };
}

export function deleteElement(resource, id, params = {}, hash = false) {
    const restClient = getRestClient();
    hash = hash ? hash : generateHash(resource, params);

    if (logger) {
        console.group('REST Action - deleteElement'); // eslint-disable-line
        console.log('resource', resource); // eslint-disable-line
        console.log('params', params); // eslint-disable-line
        console.groupEnd(); // eslint-disable-line
    }

    return (dispatch) => {
        return new Promise ((resolve, reject) => {
            restClient('DELETE', resource, params)
                .then(json => {
                    dispatch(invalidateApi(hash));
                    resolve(json);
                })
                .catch((json) => reject(json));
        });
    };
}
