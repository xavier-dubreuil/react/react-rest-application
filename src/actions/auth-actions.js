import {getAuthClient} from '../application';
import {invalidateAll} from './data-actions';

export const REQUEST_AUTH = 'REQUEST_AUTH';
export const RECEIVE_AUTH = 'RECEIVE_AUTH';
export const INVALIDATE_AUTH = 'INVALIDATE_AUTH';

export function authenticate(username, password) {

    const authClient = getAuthClient();
    // console.group('REST Action - authenticate');
    // console.log('username', username);
    // console.log('password', password);
    // console.groupEnd();

    return authClient('AUTH_LOGIN', {
        data: {
            username: username,
            password: password,
        }
    });
}

export function disconnect() {
    // console.group('REST Action - disconnect');
    // console.groupEnd();

    return (dispatch) => {
        const authClient = getAuthClient();

        dispatch(invalidateAll());
        return authClient('AUTH_LOGOUT', {});
    };
}
